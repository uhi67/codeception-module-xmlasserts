XMLAsserts
==========

Codeception Module for testing XML structures.

Version 1.0

Installation
------------

`composer require "uhi67/codeception-module-xmlasserts:^1.0"` 

Usage
-----

1. Include in your suite's yml:

```yaml
modules:
    enabled:
        - XmlAsserts
```

2. Build codeception generated classes:
   
    `codecept build`

3. Use assertions in tests/cests

```php
    /** @noinspection PhpUndefinedVariableInspection */
    $I->assertXmlMatches($xpath, $xml);      // Checks XML matches XPath query
    $I->assertXmlIncludes($fragment, $xml);  // Checks if XML includes provided XML fragment.
```

... and helper methods

```php
    /** @noinspection PhpUndefinedVariableInspection */
    $xml = $I->toXml($value);                 // Converts string, array or other node to DOMDocument
    $result = $I->xmlEval($query, $xml);      // Evaluates an Xpath query, returns typed result or nodeset
    $result = $I->xmlQuery($query, $xml);      // Evaluates an Xpath query, returns nodeset only     
```

## License

**uhi67/codeception-module-xmlasserts** is released under the MIT License. See the bundled `LICENSE` for details.
