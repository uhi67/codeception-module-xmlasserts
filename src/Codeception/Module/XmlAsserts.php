<?php /** @noinspection PhpUnused */

namespace Codeception\Module;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module as CodeceptionModule;
use Codeception\TestInterface;
use Codeception\Util\Xml;
use Codeception\Util\XmlBuilder;
use DOMDocument;
use DOMNode;
use DOMNodeList;
use DOMXPath;

/**
 * Module for testing XML structures.
 */
class XmlAsserts extends CodeceptionModule implements DependsOnModule
{
    public function _before(TestInterface $test)
    {
    }

    protected function onReconfigure()
    {
    }

    public function _depends()
    {
        return [];
    }

    /**
     * Checks if XML includes provided XML fragment.
     * Comparison is done by canonicalizing both xml`s (except fragment if string)
     * Parameter can be passed either as XmlBuilder, DOMDocument, DOMNode, XML string, or array (if no attributes).
     *
     * @param string|\DOMDocument|\DOMNode|\Codeception\Util\XmlBuilder $fragment
	 * @param string|\DOMDocument|\DOMNode|\Codeception\Util\XmlBuilder $xml
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection -- codecept module must have FQCNs in phpdoc
     */
    public function assertXmlIncludes($fragment, $xml)
    {
        $xml = $this->canonicalize($xml);
		if(!is_string($fragment)) $fragment = $this->canonicalize($fragment);
        $this->assertStringContainsString($fragment, $xml, "found in XML Response");
    }


	/**
	 * Checks XML matches XPath locator
	 *
	 * @param string $query -- the xpath query expression
	 * @param string|\DOMDocument|\DOMNode|\Codeception\Util\XmlBuilder $xml -- the xml to search in
     * @param array $namespaces -- namespaces to register to xpath query [alias=>url,...]
     * @param string $message -- optional user message if assertion fails
	 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection -- codecept module must have FQCNs in phpdoc
	 */
    public function assertXmlMatches($query, $xml, $namespaces=[], $message='')
    {
        $xpath = new DOMXPath(Xml::toXml($xml));
        foreach($namespaces as $prefix=>$namespace) {
            $xpath->registerNamespace($prefix, $namespace);
        }
        /** @var DOMNodeList|false $res */
        $res = $xpath->query($query);
        $this->assertTrue($res!==false && $res->length>0, 'XML not matches the xpath query ('.$query.'). '.$message);
    }

    protected function canonicalize($xml)
    {
        return Xml::toXml($xml)->C14N();
    }

    /**
     * Converts string, array or other node to DOMDocument
     *
     * @param \Codeception\Util\XmlBuilder|\DOMDocument|\DOMNode|array|string $xml
     * @return \DOMDocument|false -- false on failure (invalid xml string)
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection -- codecept module must have FQCNs in phpdoc
     */
    public static function toXml($xml)
    {
        if ($xml instanceof XmlBuilder) {
            return $xml->getDom();
        }
        if ($xml instanceof DOMDocument) {
            return $xml;
        }
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        if ($xml instanceof DOMNode) {
            $xml = $dom->importNode($xml, true);
            $dom->appendChild($xml);
            return $dom;
        }

        if (is_array($xml)) {
            return Xml::arrayToXml($dom, $dom, $xml);
        }
        if (!empty($xml)) {
            $result = $dom->loadXML($xml);
            if(!$result) return false;
        }
        return $dom;
    }

    /**
     * Evaluates an Xpath query, returns typed result or nodeset
     *
     * @param string $query
     * @param string|\DOMDocument|\DOMNode|\Codeception\Util\XmlBuilder $xml
     * @return \DOMNodeList|string|integer|bool -- typed result or nodeset
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection -- codecept module must have FQCNs in phpdoc
     */
    public static function xmlEval($query, $xml=null, $namespaces=[]) {
        $node = $xml instanceof DOMNode ? $xml : null;
        $document = $xml instanceof DOMDocument ? $xml : ($xml instanceof DOMNode ? $xml->ownerDocument : Xml::toXml($xml));
        $xpath = new DOMXPath($document);
        foreach($namespaces as $prefix=>$namespace) {
            $xpath->registerNamespace($prefix, $namespace);
        }
        return $xpath->evaluate($query, $node);
    }

    /**
     * Evaluates an Xpath query, returns nodeset
     *
     * @param string $query
     * @param string|\DOMDocument|\DOMNode|\Codeception\Util\XmlBuilder $xml
     * @return \DOMNodeList|bool -- nodeset or false on error
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection -- codecept module must have FQCNs in phpdoc
     */
    public static function xmlQuery($query, $xml=null, $namespaces=[]) {
        $node = $xml instanceof DOMNode ? $xml : null;
        $document = $xml instanceof DOMDocument ? $xml : ($xml instanceof DOMNode ? $xml->ownerDocument : Xml::toXml($xml));
        $xpath = new DOMXPath($document);
        foreach($namespaces as $prefix=>$namespace) {
            $xpath->registerNamespace($prefix, $namespace);
        }
        return $xpath->query($query, $node);
    }
}

